package sentenceTagger

import edu.stanford.nlp.tagger.maxent.MaxentTagger
import scala.io.Source

object SentTagger extends App {

  val tagger = new MaxentTagger(
    this.getClass().getResource(
      "/tagger/bidirectional-distsim-wsj-0-18.tagger").getPath())

  val trainSet = Source.fromFile(this.getClass().getResource("/productsDataSet.csv").getPath())
  trainSet.getLines.toList.foreach(line => {
    val
    println(tagger.tagString(line).split("\\s")(0))
  })

  trainSet.reader.close()

  val findProperNoun = (str: String) =>
    if(str.split("/").eq("NNP")) str.split("/")(0) else None


  def findBrand(name: String, fn: String => String) =
    fn(name)


  // list of combinations to look for 

  // NN - VB
  // VB - NN
  // NN* - CD
  // NN - NN*
  // NN - FW*
  // FW* - NN 

}